/* 
 * Copyright 2010 Aalto University, ComNet
 * Released under GPLv3. See LICENSE.txt for details. 
 */
package report;

import core.DTNHost;
import core.Message;
import core.Settings;
import core.UpdateListener;
import routing.community.SimBetWFairRoute;
import routing.simbet.SimBetWithFairRouting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Report for generating different kind of total statistics about message
 * relaying performance. Messages that were created during the warm up period
 * are ignored.
 * <P><strong>Note:</strong> if some statistics could not be created (e.g.
 * overhead ratio if no messages were delivered) "NaN" is reported for
 * double values and zero for integer median(s).
 */
public class MatrixReport extends Report implements UpdateListener {


	//private String[] matrixEgoNetwork = SimBetWithFairRouting;
	private int interval;
	public static final String MATRIX_REPORT_INTERVAL = "matrixInterval";
	public static final int DEFAULT_MATRIX_REPORT_INTERVAL = 3600;

	/**
	 * Constructor.
	 */
	public MatrixReport() {
		super();

		Settings settings = getSettings();
		if (settings.contains(MATRIX_REPORT_INTERVAL)) {
			interval = settings.getInt(MATRIX_REPORT_INTERVAL);
		} else {
			interval = -1; /* not found; use default */
		}

		if (interval < 0) { /* not found or invalid value -> use default */
			interval = DEFAULT_MATRIX_REPORT_INTERVAL;
		}
	}

	@Override
	protected void init() {
		super.init();

		

	}

	
	public void messageDeleted(Message m, DTNHost where, boolean dropped) {

	}

	
	public void messageTransferAborted(Message m, DTNHost from, DTNHost to) {

	}

	
	public void messageTransferred(Message m, DTNHost from, DTNHost to,
			boolean finalTarget) {

	}


	public void newMessage(Message m) {

	}
	
	
	public void messageTransferStarted(Message m, DTNHost from, DTNHost to) {

	}
	

	@Override
	public void done() {
//		write("Message stats for scenario " + getScenarioName() +
//				"\nsim_time: " + format(getSimTime()));
//		double deliveryProb = 0; // delivery probability
//		double responseProb = 0; // request-response success probability
//		double overHead = Double.NaN;	// overhead ratio
//
//		if (this.nrofCreated > 0) {
//			deliveryProb = (1.0 * this.nrofDelivered) / this.nrofCreated;
//		}
//		if (this.nrofDelivered > 0) {
//			overHead = (1.0 * (this.nrofRelayed - this.nrofDelivered)) /
//				this.nrofDelivered;
//		}
//		if (this.nrofResponseReqCreated > 0) {
//			responseProb = (1.0* this.nrofResponseDelivered) /
//				this.nrofResponseReqCreated;
//		}
		

			;
		

		super.done();
	}


    public void connectionUp(DTNHost thisHost) {
   }

	@Override
	public void updated(List<DTNHost> hosts) {

	}
}
